package ESG;


import java.util.Random;

public interface Constantes {
 
    public static final int RETRASO = 1;
    
    public static final int MIN_TIEMPO_FOTOGRAMA = 1;
    public static final int INC_TIEMPO_FOTOGRAMA = 3;
    
    public static final int MIN_NUM_FOTOGRAMAS = 1;
    public static final int VARIACION_NUM_FOTOGRAMAS = 3;
    
    public static final int MIN_NUM_ESCENAS = 2;
    public static final int VARIACION_NUM_ESCENAS = 3;
    
    public static final int MIN_TIEMPO_GENERACION = 1;
    public static final int VARIACION_TIEMPO_GENERACION = 2;
    
    public static final int MAX_ESCENAS_EN_ESPERA = 30;
   // public static final int INC_ESCENAS_EN_ESPERA = 5;
    
    public static final int NUM_GENERADORES = 3;
    public static final int NUM_RENDERIZADORES = 3;
    
    public static final int TIEMPO_FINALIZACION_ESCENA = 2;
    
    public final static Random generador = new Random();

}
