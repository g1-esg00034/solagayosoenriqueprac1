package ESG;


import static ESG.Constantes.MIN_NUM_ESCENAS;
import static ESG.Constantes.MIN_TIEMPO_GENERACION;
import static ESG.Constantes.VARIACION_NUM_ESCENAS;
import static ESG.Constantes.VARIACION_TIEMPO_GENERACION;
import static ESG.Constantes.generador;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Generador implements Callable<List<Escena>>
{
    //IDENTIFICADOR
    static int ident=0;    
    //CADENA DE IDENTIFICADOR
    String cadena;
    
    //LISTAS COMPARTIDAS
    List<Escena> resultado;
    List<Escena> listaAltaPrioridad;
    List<Escena> listaBajaPrioridad;
    
    //SEMÁFOROS ( DUDA DE SI VAN AQUÍ O EN EL MAIN O SE PASAN POR CABECERA O QUE)
    Semaphore LlenarAltaPrioridad;
    Semaphore EXCMAltaPrioridad;
    Semaphore VaciarAltaPrioridad;
    Semaphore LlenarBajaPrioridad;
    Semaphore EXCMBajaPrioridad;
    Semaphore VaciarBajaPrioridad;
    
    //FINALIZADOR
    Finalizador finalizador;
    
    //CONTROL DEL MARCO EJECUTOR
    CyclicBarrier barrier;
    
public Generador( Semaphore AltaPrioridad,Semaphore VAltaPrioridad,List<Escena> ListaAltaPrioridad,Semaphore BajaPrioridad,Semaphore VBajaPrioridad,List<Escena> ListaBajaPrioridad, CyclicBarrier barrera, Finalizador fin )
{
    ident++;
    cadena=">" + ident + "<";
    
    resultado=new LinkedList<>();
    listaAltaPrioridad= ListaAltaPrioridad; //CONTROL DEL ACCESO A LA LISTA
    listaBajaPrioridad= ListaBajaPrioridad; //SAME
    
    LlenarAltaPrioridad= AltaPrioridad; //ESTAMOS VACIOS EN ALTA PRIORIDAD, A LLENAR
    EXCMAltaPrioridad= new Semaphore(1); //ACCESO O NO ACCESO
    VaciarAltaPrioridad = VAltaPrioridad; //ESTAMOS LLENOS EN ALTA PRIORIDAD, A VACIAR
      
    LlenarBajaPrioridad= BajaPrioridad; //IDEM
    EXCMBajaPrioridad= new Semaphore(1); //ACCESO O NO ACCESO
    VaciarBajaPrioridad = VBajaPrioridad; //IDEM
    
    barrier= barrera; //MARCO EJECUTOR, CONTROL DE BARRERA Y FINALIZADOR
    finalizador = fin;
}

   @Override
   public List<Escena> call() throws Exception  {
    int variacion = generador.nextInt(VARIACION_NUM_ESCENAS);
    int NumEscenas= MIN_NUM_ESCENAS+variacion;
    System.out.println("Iniciando la ejecución de " + this.toString() );
    try{
        for (int escenas=1; escenas<=NumEscenas; escenas++)
        {
            int _variacion = +generador.nextInt(VARIACION_TIEMPO_GENERACION);
            int TiempoEscenas= MIN_TIEMPO_GENERACION + _variacion; 
            
            try 
            {
                TimeUnit.SECONDS.sleep(TiempoEscenas);
            }catch (InterruptedException exception)
            {
                Logger.getLogger(Generador.class.getName()).log(Level.SEVERE, null, exception);
            }
            
            Escena e= new Escena();
            System.out.println("Escena: "+e.getCadena()+ "ha sido creada! \n");
            e.inicializaEscena();
            resultado.add(e);
            //AQUI SE IMPLEMENTA LA RESOLUCIÓN DE PSEUDOCÓDIGO
            if (e.getPrioridad() == true){ //LIBERACIÓN DE RECURSOS PARA TRABAJAR EN EXCLUSIÓN MUTUA Y CONTROL DE RECURSOS (BLOQUEO)
                    LlenarAltaPrioridad.acquire(); //CERRAMOS SEMAFORO DE GENERACION ALTA PRIORIDAD
                    EXCMAltaPrioridad.acquire(); //PERMITIMOS LA ESCRITURA EN EL RECURSO COMPARTIDO BLOQUEANDO AL RESTO
                    listaAltaPrioridad.add(e); //AÑADIMOS LA ESCENA A LA LISTA
                    EXCMAltaPrioridad.release(); //LIBERAMOS NUESTRO GENERADOR DE ALTA PRIORIDAD
                    VaciarAltaPrioridad.release(); //VOLVEMOS A LIBERAR EL RECURSO COMPARTIDO

            }
            if (e.getPrioridad() == false){ //LO MISMO DE ARRIBA PERO PARA PRIORIDAD BAJA
                    LlenarBajaPrioridad.acquire();
                    EXCMBajaPrioridad.acquire();
                    listaBajaPrioridad.add(e);
                    EXCMBajaPrioridad.release(); 
                    VaciarBajaPrioridad.release();
            }        
        }
        
        if(barrier.getNumberWaiting()==0)
        {finalizador.run();}
    
        this.barrier.await(); //PONEMOS EN ESPERA AL MARCO RESPECTO AL RESTO DE PROCESOS
           
   }catch(InterruptedException exception )
   { 
        System.out.println("Interrupción en: "+this.toString());
   }finally
   {
        System.out.println("Finalización de: "+this.toString());
        return resultado;
   }   
  
}
    @Override
    public String toString(){
        return "Generador"+ cadena;
    }
    
}