package ESG;

import java.util.LinkedList;
import java.util.List;

public class Finalizador implements Runnable
{
    List<Renderizador> ListaCompartida;
    
    public Finalizador( LinkedList<Renderizador> renders )
    {ListaCompartida= renders;}
    
    @Override
    public void run()
    {
        ListaCompartida.forEach(
        (ren) -> 
        {
            ren.setTerminadosGeneradores();
        }
        );
    }
    
    
}
