package ESG;

import static ESG.Constantes.INC_TIEMPO_FOTOGRAMA;
import static ESG.Constantes.MIN_TIEMPO_FOTOGRAMA;
import static ESG.Constantes.generador;

public class Fotograma {
    //IDENTIFICADOR
    static int ident=0;
    //CADENA DE IDENTIFICADOR
    private final String cadena;
    //TIEMPO DE DURACIÓN
    private int duracion;
    
    final int incremento()
    {return generador.nextInt(INC_TIEMPO_FOTOGRAMA);}
    
    public Fotograma()
    {
        ident = ident + 1;
        cadena=">" + ident + "<";
        int min = MIN_TIEMPO_FOTOGRAMA;
        duracion= min + incremento();  
    }

    public String getCadena()
    {return cadena;}

    public int getDuracion(){return duracion;}

    public void setDuracion(int _duracion)
    {duracion = _duracion;}
    
    @Override
     public String toString()
     {
         return "El fotograma: " + cadena + " ha trabajado: " + duracion;
     }
}
