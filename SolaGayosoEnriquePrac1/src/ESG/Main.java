package ESG;


import static ESG.Constantes.MAX_ESCENAS_EN_ESPERA;
import static ESG.Constantes.NUM_GENERADORES;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BrokenBarrierException;
import java.util.concurrent.CyclicBarrier;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class Main {

    static void Funcionando()
    {
        LlenarAltaPrioridad= new Semaphore(MAX_ESCENAS_EN_ESPERA); 
        VaciarAltaPrioridad= new Semaphore(0);
        listaAltaPrioridad= new LinkedList<>();
        LlenarBajaPrioridad= new Semaphore(MAX_ESCENAS_EN_ESPERA); 
        VaciarBajaPrioridad= new Semaphore(0);
        listaBajaPrioridad= new LinkedList<>();  
    }
    
    public static void Generar_generadores(LinkedList<Generador> v, CyclicBarrier barrera, Future<List<Escena>> iniciales, ExecutorService principal, Finalizador termina)
    {
        for (int i = 0; i < NUM_GENERADORES; i++) 
        {
                v.add(new Generador(LlenarAltaPrioridad,VaciarAltaPrioridad,listaAltaPrioridad,LlenarBajaPrioridad,VaciarBajaPrioridad,listaBajaPrioridad, barrera , termina));
                iniciales = principal.submit( new Generador(LlenarAltaPrioridad,VaciarAltaPrioridad,listaAltaPrioridad,LlenarBajaPrioridad,VaciarBajaPrioridad,listaBajaPrioridad, barrera, termina));
        }
    }
    
    public static void Generar_renderizadores(LinkedList<Renderizador> v, CyclicBarrier barrera)
    {
        for (int i = 0; i < NUM_GENERADORES; i++)
        {
                v.add(new Renderizador(LlenarAltaPrioridad,VaciarAltaPrioridad,listaAltaPrioridad,LlenarBajaPrioridad,VaciarBajaPrioridad,listaBajaPrioridad));
        }
    }
     
    //variables Compartidas, DUDA PLANTEADA EN GENERADOR
    static List<Escena> listaAltaPrioridad;
    static List<Escena> listaBajaPrioridad;
    
    static Semaphore LlenarAltaPrioridad;
    static Semaphore VaciarAltaPrioridad;
    static Semaphore LlenarBajaPrioridad; 
    static Semaphore VaciarBajaPrioridad;
    
        
    public static void main(String[] args) throws InterruptedException, ExecutionException, BrokenBarrierException, TimeoutException {
    
        System.out.println("Ha iniciado la ejecución el Hilo(PRINCIPAL)");
        
        //PREPARAMOS LAS ESTRUCTURAS Y LOS DATOS A TRABAJAR
        LinkedList<Generador> generadores = new LinkedList();
        LinkedList<Renderizador> renderizadores = new LinkedList();
        
        //PUESTA A PUNTO, TODOS LOS PROCESOS GENERALES
        Funcionando();
        ExecutorService principal = (ExecutorService) Executors.newCachedThreadPool();
        CyclicBarrier barrier= new CyclicBarrier(NUM_GENERADORES);
        Finalizador termina= new Finalizador(renderizadores); 
        
        //COMENZAMOS CON EL MEJUNGE
        Future<List<Escena>> iniciales = null;
        Generar_generadores(generadores , barrier , iniciales , principal, termina );
        Generar_renderizadores(renderizadores , barrier );
        
        //PREPARAMOS EL TRATAMIENTO DE DATOS AL FINAL DE LA EJECUCIÓN
        List<Future<List<Escena>>> escenasIni = principal.invokeAll( generadores);
        Future<?> fin_Finalizador=principal.submit(termina);   
        List<Future<List<Escena>>> escenasFin = principal.invokeAll( renderizadores);                                                                                                         
        
        //APAGAMOS LAS MAQUINAS
        principal.shutdown();      
        principal.awaitTermination(10, TimeUnit.SECONDS);
        
        //A PARTIR DE AQUI; SOLO TRATAMOS LOS DATOS TRABAJADOS
        
        float result = 0;
        int n_escenas=0;
        
        for (Future<List<Escena>> escena : escenasFin) 
        {
             List<Escena> lista=escena.get();
             n_escenas+=lista.size();
             for(int i = 0; i<lista.size(); i++)
             {
                result+=lista.get(i).TotalProcesamiento();
                System.out.println("La escena " + lista.get(i).getCadena() + " generó: " + lista.get(i).toString());
             }
        }
           
        
        
        System.out.println("Trabajo(procesamiento total): "+ result );
        System.out.println("Nº escenas: "+ n_escenas);
        System.out.println("Ha finalizado la ejecución el Hilo(PRINCIPAL)");
    }      
    
}
