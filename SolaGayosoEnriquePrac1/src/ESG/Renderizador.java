package ESG;


import static ESG.Constantes.RETRASO;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

public class Renderizador implements Callable<List<Escena>>{
    
    //IDENTIFICADOR
    static int ident=0;
    //CADENA SALIDA
    private final String cadena;
    //TERMINAMOS??
    private boolean finalizador;
    
    //LISTAS COMUNES
    List<Escena> resultados;
    List<Escena> AltaPrioridad;
    List<Escena> BajaPrioridad;
    
    //SEMÁFOROS
    Semaphore Mutex;
    Semaphore LlenarAltaPrioridad;
    Semaphore EXCMAltaPrioridad;
    Semaphore VaciarAltaPrioridad;
    Semaphore LlenarBajaPrioridad;
    Semaphore EXCMBajaPrioridad;
    Semaphore VaciarBajaPrioridad;
    
  public Renderizador(Semaphore LAltaPrioridad,Semaphore VAltaPrioridad,List<Escena> AltaPrioridad,Semaphore LBajaPrioridad,Semaphore VBajaPrioridad,List<Escena> BajaPrioridad ){
    
    ident++;
    cadena=">" + ident + "<";
    finalizador= false; 
    
    resultados=new LinkedList<>();
    this.AltaPrioridad= AltaPrioridad;
    this.BajaPrioridad= BajaPrioridad;
    
    Mutex = new Semaphore(1);
    LlenarAltaPrioridad= LAltaPrioridad;
    VaciarAltaPrioridad = VAltaPrioridad;
    LlenarBajaPrioridad= LBajaPrioridad;
    VaciarBajaPrioridad = VBajaPrioridad;

    EXCMAltaPrioridad= new Semaphore(1);
    EXCMBajaPrioridad= new Semaphore(1);
  }

  @Override
  public List<Escena> call() throws InterruptedException {
    Escena escena= null;
    boolean tengoEscena;
    while (true){
        tengoEscena= false;
        Mutex.acquire();
        if(!AltaPrioridad.isEmpty())//SI ESTÁ VACÍA, SE PUEDEN PONER A TRABAJAR SOLO PROCESOS DE LECTURA Y ESCRITURA SOLO DE AÑADIR
        { 
            VaciarAltaPrioridad.acquire();
            EXCMAltaPrioridad.acquire();
            escena= AltaPrioridad.get(0);
            AltaPrioridad.remove(0);
            EXCMAltaPrioridad.release();
            LlenarAltaPrioridad.release();
            tengoEscena= true;
        }
        if(AltaPrioridad.isEmpty())//LO CONTRARIO
        { 
            if(!BajaPrioridad.isEmpty())
            {
                VaciarBajaPrioridad.acquire();
                EXCMBajaPrioridad.acquire();
                escena= BajaPrioridad.get(0);
                BajaPrioridad.remove(0);
                EXCMBajaPrioridad.release();
                LlenarBajaPrioridad.release();
                tengoEscena= true;
            }if(BajaPrioridad.isEmpty())
            {
                if(finalizador)
                {
                    Mutex.release();
                    return resultados;
                }
            }
        }
        
        Mutex.release();
        if (tengoEscena)
        {
            escena.setInicio(new Date());
            TimeUnit.SECONDS.sleep(escena.getDuracion());
            escena.setFin(new Date());
            resultados.add(escena);
        }else
        {
            TimeUnit.SECONDS.sleep(RETRASO);
        }        
    }
      
  }
  
  public void setTerminadosGeneradores(){
      finalizador= true;
  }

}