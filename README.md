 
PRÁCTICA 01
Resolución con semáforos
 
 
RESOLUCIÓN TEÓRICA
Primero de todo, resolveremos la práctica como hacemos en los ejercicios hechos en clase. Justificaremos adecuadamente las decisiones. Dado que es la práctica 01, estaremos obligados a usar semáforos y no otra herramienta para su resolución. 

ANÁLISIS DE LA PRÁCTICA A REALIZAR
Se nos presenta un problema de renderizado de escenas, compuestas por fotogramas y con ejecución puramente concurrente. La granja de renderizado, tiene dos tipos de procesos que gestionan dichas escenas:
-	Generadores. Son capaces de crear escenas de dos tipos de prioridad, alta y baja. Dichas escenas serán almacenadas en 2 listas de escenas diferentes de tamaño limitado.
-	Renderizadores. Procesarán las escenas en orden de mayor a menor prioridad. Esto sugiere que las listas de escenas serán compartidas por estos dos procesos.
Los elementos a tener en cuenta son:
	Fotogramas. Tienen un identificador único por cada fotograma y un tiempo de cálculo aleatorio entre MIN_TIEMPO_FOTOGRAMA y MIN_TIEMPO_FOTOGRAMA + VARIACION_TIEMPO segundos.
	Escenas. Tienen un identificador único para cada escena. Un atributo para controlar la prioridad, alta o baja, de la escena, que indicará si se atenderán antes o después por los renderizadores. Solo cuando no haya escenas de prioridad alta, se atienden las de prioridad baja. Por definición, una escena es un conjunto de fotogramas con tamaño aletorio entre MIN_NUM_FOTOGRAMAS y MIN_NUM_FOTOGRAMAS + VARIACION_NUM_FOTOGRAMAS. El tiempo que se tarda en renderizar una escena, es la suma del tiempo de cada fotograma, más un tiempo fijo llamado TIEMPO_FINALIZACION_ESCENA.
	Generadores. Crean una escena y la ponen a disposición de los renderizadores. Un generador tarda un tiempo aleatorio entre MIN_TIEMPO_GENERACION Y MIN_TIEMPO_GENERACION + VARIACION_TIEMPO segundos en generar la escena.
	Renderizadores. Toma una escena de las que hay disponibles y la renderizan. Siempre que haya, elige primero de la cola de prioridad alta, si no hay en esa cola, las coge de la de prioridad baja. Una escena no está completa hasta que no se han renderizado todos sus fotogramas. Cuando una escena ha sido renderizada, se coloca en una lista de escenas resultado.


Los generadores de escenas se comunican con los renderizadores de escenas mediante dos listas de escenas con acceso común. Hay dos listas según la prioridad. Los generadores escriben escenas en las listas y los renderizadores las leen. ( Problema clásico de “Productor y consumidor” ). Esas listas tienen un tamaño máximo de MAX_ESCENAS_EN_ESPERA. Si ya hay MAX_ESCENAS_EN_ESPERA en una lista y un generador tiene que escribir en ella, deberá esperar a que salga aluna escena antes de poder seguir escribiendo en la lista.
Proceso general
Se construirán NUM_GENERADORES Generadores de escenas. Se construirán NUM_RENDERIZADORES Renderizadores de escenas. Cada Generador de escenas construirá un número de escenas comprendido entre MIN_NUM_ESCENAS y MIN_NUM_ESCENAS + VARIACION_NUM_ESCENAS. 
Cuando los generadores han generado todas sus escenas, terminan su ejecución. Cuando no quedan escenas en ninguna de las listas y los generadores han terminado su ejecución, los renderizadores terminan su ejecución. 
Al finalizar la ejecución de todas las tareas, el programa mostrará: 
•	El número total de escenas procesadas. 
•	El tiempo total empleado en Renderizar todas las escenas. 
Para cada escena: 
-	La hora en que se generó. 
-	La hora en que un Renderizador comenzó su procesamiento. 
-	Los fotogramas que la componen y la duración de cada uno.
-	La hora en la que el Renderizador terminó su procesamiento. 
-	El tiempo total empleado en el procesamiento de la escena.


CLASES , MÉTODOS Y ESTRUCTURAS DE DATOS
	ESTRUCTURAS DE DATOS

-  Buffer <TipoDato> : Estructura que va a permitir almacenar elementos de TipoDato, probablemente implementada como una lista o un array. Esta estructura recuerda a la utilizada en los problemas tipo productor y consumidor, ya que mantiene un orden en la inserción y la extracción.

•	MÉTODOS O FUNCIONES
o	ADD(TipoDato): inserción ordenada en el Buffer del elemento TipoDato.
o	GET(): TipoDato. Devuelve en orden de extracción el elemento de la estructura de datos y lo elimina de ésta.
o	EMPTY(): Boolean. Nos dice si el buffer está vacío o no.

-  BufferAltaPrioridad <Escena> : Al igual que el buffer anterior, utilizaremos esta estructura para almacenar elementos siguiendo un orden, en este caso las escenas de prioridad alta. Los métodos y funciones serán los mismos de la estructura.
-  BufferBajaPrioridad <Escena> : Lo mismo que el buffer anterior pero para escenas de baja prioridad.

	CLASES

-  Fotograma : Clase para controlar los fotogramas generados.

•	ATRIBUTOS
o	IDFotograma: int.
o	Tiempo: int.
•	MÉTODOS O FUNCIONES
o	GetTiempoCalculo(): Devuelve el tiempo de cálculo del fotograma.

-  Escena : Clase para controlar los fotogramas generados.

•	ATRIBUTOS
o	IDEscena: int.
o	TiempoTotal: int.
o	Prioridad: Boolean. True = alta , False = baja.
o	BufferFotogramas: Buffer<Fotograma>.
•	MÉTODOS O FUNCIONES
o	GeneraFotogramas(): Crea los fotogramas de la escena adecuada.
o	FinalizaFotograma(): Tiempo finalización fotogramas.


-  Generador : Clase para generar escenas.

•	ATRIBUTOS
o	Tiempo: int. Tiempo recopilación fotogramas.
•	MÉTODOS O FUNCIONES
o	GeneraEscenas(): Método principal. Genera todas las escenas.

-  Procesador : Clase para procesar las escenas.

•	ATRIBUTOS
o	TiempoTotal: int. Tiempo total de procesar una escena.
•	MÉTODOS O FUNCIONES
o	ProcesaEscena(): Lee y procesa las escenas.

-  Buffers : Clase para controlar los buffer y las estructuras que los trabajan.

•	ATRIBUTOS
o	BufferAlta: Buffer<Escena>. Buffer de escenas de alta prioridad.
o	BufferBaja: Buffer<Escena>. Buffer de escenas de baja prioridad.

-  Principal : Clase principal que lanzará todos los recursos y controlará la ejecución y finalización del resto de procesos y clases.

•	ATRIBUTOS
o	Buffer: Buffers.
•	MÉTODOS O FUNCIONES
o	EjecutaGeneradores(). Crea los generadores de escenas y los lanza.
o	EjecutaRenderizadores(). Crea los renderizadores y los lanza.
o	FinalizaGeneradores(). Controla y finaliza los generadores lanzados anteriormente.
o	FinalizaRenderizadores(). Controla y finaliza los renderizadores lanzados anteriormente.

-  Finalizador : Clase que se encargará de recopilar al información requerida en este ejercicio de la práctica para poder presentársela al usuario.

•	ATRIBUTOS
o	TiempoTotal: int.
o	FechaGeneración: Date.
o	FechaInicioProceso: Date.
o	FechaFinProceso: Date.
o	BufferResultado: Buffer <Escena>.
•	MÉTODOS O FUNCIONES
o	TiempoTotalEscena(): int. Tiempo total de la escena. 


VARIABLES COMPARTIDAS
Cuando tratamos con concurrencia como en este caso, muchas veces existen recursos como la memoria, que son compartidos por varios procesos, por lo que un proceso no podrá estar escribiendo en una zona de memoria si un proceso está leyendo esa misma zona de memoria en el mismo instante. En el caso de nuestra práctica, existen recursos compartidos entre las clases Generador, Procesador y la Principal, dichos recursos son:
-	BufferAlta: Buffer<Escena>. Almacena las escenas de prioridad alta donde escribirán los generadores ( para meter las escenas generadas) y leerán los renderizadores ( para sacar las escenas y renderizarlas ).
-	BufferBaja: Buffer<Escena>. Almacena las escenas de prioridad baja donde escribirán los generadores ( para meter las escenas generadas) y leerán los renderizadores ( para sacar las escenas y renderizarlas ).

SEMÁFOROS
Para evitar el problema que se nos plantea en el enunciado anterior, plantearemos semáforos de exclusión mutua, que permiten compartir recursos entre procesos sin problema ninguno. Aunque no es el único problema que se nos plantea en esta práctica, por lo que habrá que implementar algunos semáforos más.
	PrioridadAltaVacía: Este semáforo regulará el acceso de los generadores al buffer de escenas de alta prioridad. Si la lista está vacía o hay algún hueco, podrán acceder, si está llena, no. El semáforo representará el número de posiciones libres en dicho buffer por lo que tendrá que inicializarse a MAX_ESCENAS_EN_ESPERA.

	PrioridadAltaLlena: Semáforo totalmente contrario al anterior, en este caso regulará el acceso de los procesadores, que si la lista está vacía, no podrán acceder, pero si está llena o hay algún elemento, sí. Como este semáforo representa el número de elementos en el buffer, al contrario que el anterior, se inicializará en cero.

	PrioridadBajaVacía: Lo mismo que PrioridadAltaVacía pero para la prioridad baja.

	PrioridadBajaLlena: Lo mismo que PrioridadAltaLlena pero para la prioridad baja.

Estos son los semáforos que he comentado antes para el acceso a los recursos compartidos, serán semáforos de exclusión mutua por lo tanto:

	EXCMBufferAlta: Necesario para garantizar el acceso seguro al buffer de prioridad alta. Se inicializa en uno.
	EXCMBufferBaja: Necesario para garantizar el acceso seguro al buffer de prioridad baja. Se inicializa en uno.


ESPECIFICACIONES DE PROCEDIMIENTOS
En el análisis anterior de las clases y sus métodos, hemos sido escuetos en cuanto al funcionamiento de los distintos procesos. Como el último paso para concluir nuestro informe de análisis y diseño es el de implementar el pseudocódigo de la práctica, es conveniente que previamente entremos un poco más en profundidad en el funcionamiento de estos métodos y procesos, hablando de ellos como procesos. Además también mencionaremos aspectos no tratados anteriormente.
En el proceso Principal (hilo principal):
	CrearProceso( Generadores, Procesadores ): Creará los procesos Generador y Procesador.
	EjecutarProceso( Generadores, Procesadores ): Ejecutará los procesos correspondientes y los lanzará.
	EsperarProceso( Escenas, Procesadores): Esperará a que todos los fotogramas de todas las escenas correspondientes hayan sido finalmente procesados.
	FinalizarProceso( Generadores, Procesadores): Finalizará todos los procesos del sistema.
En el proceso Escena:
	GeneraFotogramas(): crea un número aleatorio de fotogramas entre MIN_NUM_FOTOGRAMAS y MIN_NUM_FOTOGRAMAS + VARIACION_NUM_FOTOGRAMAS que representa los fotogramas que forman la Escena final.
	FinalizaFotogramas(): permite indicar el tiempo de finalización de fotogramas solicitados.
En el proceso Generador:
	GeneraEscenas(): permite crear un número de escenas entre MIN_NUM_ESCENAS y MIN_NUM_ESCENAS + VARIACION_NUM_ESCENAS.
	GeneraEscena(): genera una escena como tal y la agrega al buffer que corresponda.
En el proceso Procesador:
	ProcesaEscena(): Procesa una escena de la buffer correspondiente según prioridades.
En el proceso Finalizador:
	MostrarDatos( Resultados ): Este procedimiento presenta de forma legible los datos solicitados.


DISEÑO Y PSEUDOCÓDIGO


PROCESO PRINCIPAL ( HILO PRINCIPAL )
Class Principal {
Semaforo PrioridadAltaVacía, PrioridadBajaVacía, PrioridadAltaLLena, PrioridadBajaLlena;
EXCMSemaforo EXCMBufferAlta, EXCMBufferBaja;

Crear Recursos;
CrearProceso( Generadores, Procesadores);
Ejecucion_Proceso( Generadores, Procesadores );
While( eserarProceso(Generadores, Procesadores))
{	Sleep		}
FinalizarProceso( Generadores, Procesadores);
MostrarDatos( Resultados );
}
	


PROCESO ESCENA
Class Escena {
	Int ID;
	Boolean Prioridad;
	Int TiempoTotal;
	Buffer<Fotograma> BufferFotogramas;
	EXCMSemaforo EXCMBufferFotogramas;

	Ejecución() {
	GeneraFotogramas();
	FinalizaFotogramas();
}

GeneraFotogramas() {
	For( i = 1 , i < numero_fotogramas , i++)
{
	Fotograma fotograma = fotograma_aleatorio(i);
	tiempoTotal = tiempoTotal + tiempo_simulación;
	BufferFotogramas.add(fotograma);
}
}

GetTiempoTotal() {	return tiempoTotal; 	}

FinalizaFotogramas() {	Resultados.generaEscena(now()); 	}
}



PROCESO GENERADOR
Class Generador {
	Int tiempo;
	Buffer<escena> BufferAltaPrioridad;
Buffer<escena> BufferBajaPrioridad;
	

	Ejecucion_proceso() {
	generaEscenas();
}

generaEscenas() {
	for( i = 1 , i < numero_aleatorio_escenas() , i++ )
{
	Escena escena;
	If( escena.prioridadAlta )
	{
		PrioridadAltaVacía.wait();
EXCMBufferAlta.wait();
BufferAltaPrioridad.add(escena);
EXCMBufferAlta.signal();
PrioridadAltaLlena.signal();
}
Else
{
		PrioridadBajaVacía.wait();
EXCMBufferBaja.wait();
BufferBajaPrioridad.add(escena);
EXCMBufferBaja.signal();
PrioridadBajaLlena.signal();
}
Resultados.hora_generacion(now()+simulación_tiempo());
}
}
}



PROCESO PROCESADOR
Class Procesador {
	Int tiempoFinEscena;

	EjecucionProceso() {	While(!interrumpido) { procesaEscena(); } 	}
procesaEscena() {
	Resultados.hora_inicio_proceso(now());
	If(!BufferAltaPrioridad.empty())
	{
PrioridadAltaLlena.wait();
EXCMBufferAlta.wait();
Escena = BufferAltaPrioridad.get();
EXCMBufferAlta.signal();
		PrioridadAltaVacía.signal();
}
Else
{
	If(!escena.prioridadBaja.empty();)
{
PrioridadBajaLlena.wait();
EXCMBufferBaja.wait();
Escena = BufferBajaPrioridad.get();
EXCMBufferBaja.signal();
		PrioridadAltaVacía.signal();
}
}
Simula_tiempo_renderizado()+tiempoFinEscena;
Resultados.hora_fin_proceso(now());
Resultados.bufferEscenasResultados.add(escena);
}
}



PROCESO FOTOGRAMA ( ID )
Class Fotograma {
	Int ID;
	Int tiempo;

	Int getTiempo() { return tiempo; }
}


PROCESO RESULTADOS ( escena, hora_generacion , hora_inicio_proceso, hora_fin_proceso )
Class Resultados {
	Date tiempo_generacion;
	Date hora_inicio_proceso;
	Date hora_fin_proceso;
	Date hora_total_escena;
	Buffer<Fotograma> bufferEscenaResultado;

	Total_tiempo_Escena() {
		Int total;
		For i in bufferEscenaResultado 
        {
			Total = Total + i.GetTiempo();
        }
}
}










